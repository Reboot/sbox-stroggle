using Sandbox;
using Sandbox.UI;
using Sandbox.UI.Construct;

namespace Stroggle.UI;

public class Ammo : Panel
{
	public Label Label;

	public Ammo()
	{
		Label = Add.Label( "Yeehaw", "value" );
	}

	public override void Tick()
	{
		var player = Local.Pawn as StrogglePlayer;
		if ( player == null ) return;

        if ( player.ActiveChild != null)
        {
            var weapon = player.ActiveChild as StroggleWeapon;
            var clip = weapon.AmmoClip.ToString();
            var total = weapon.AvailableAmmo().ToString();
            Label.Text = clip + " / " + total;
        }
        else
        {
            Label.Text = "N/A";
        }
	}
}
