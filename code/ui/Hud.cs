﻿using Sandbox;
using Sandbox.UI;

namespace Stroggle.UI;

public class Hud : RootPanel
{
	public Hud()
	{
		Local.Hud = this;

		StyleSheet.Load( "/ui/Hud.scss" );
		AddClass( "panel" );
		AddClass( "fullscreen" );

		Init();
	}

	private void Init()
	{
		AddChild<ChatBox>();
		AddChild<VoiceList>();
		AddChild<VoiceSpeaker>();
		//RootPanel.AddChild<KillFeed>();
		AddChild<Scoreboard<ScoreboardEntry>>();
		AddChild<Health>();
		AddChild<Armor>();
		AddChild<Ammo>();
		AddChild<Currency>();
		AddChild<InventoryBar>();
		AddChild<FullscreenUI>();
		AddChild<Crosshair>();
	}

	
	[Event.Hotload]
	private void OnHotReload()
	{
		DeleteChildren( true );
		Init();
	}
}
