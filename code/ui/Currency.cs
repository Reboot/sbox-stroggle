using Sandbox;
using Sandbox.UI;
using Sandbox.UI.Construct;

namespace Stroggle.UI;
public class Currency : Panel
{
	public Label Label;

	public Currency()
	{
		Label = Add.Label( "100", "value" );
	}

	public override void Tick()
	{
		var player = Local.Pawn as StrogglePlayer;
		if ( player == null ) return;

		Label.Text = player.Currency.CeilToInt().ToString();
	}
}
