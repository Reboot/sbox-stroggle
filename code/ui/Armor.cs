using Sandbox;
using Sandbox.UI;
using Sandbox.UI.Construct;

namespace Stroggle.UI;
public class Armor : Panel
{
	public Label Label;

	public Armor()
	{
		Label = Add.Label( "100", "value" );
	}

	public override void Tick()
	{
		var player = Local.Pawn as StrogglePlayer;
		if ( player == null ) return;

		Label.Text = player.Armor.CeilToInt().ToString();
	}
}
