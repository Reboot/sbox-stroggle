using Sandbox.UI;

namespace Stroggle.UI;

[UseTemplate]
public class FullscreenUI : Panel
{
	public static FullscreenUI Instance;
	public bool IsOpen { get => ActivePanel is not null; }
	public Panel ActivePanel { get; private set; }

	private bool _isForcedOpen = false;

	public FullscreenUI()
	{
		Instance = this;
		//this.EnableFade( false );
	}

	public void ForceOpen( Panel panel )
	{
		_isForcedOpen = true;
		ActivePanel = null;

		Open( panel );
	}

	/// <summary>
	/// Ensure that the panel parameter isn't being created on each tick.
	/// </summary>
	public void Open( Panel panel )
	{
		if ( ActivePanel is not null )
			return;

		Log.Info("Opening panel");
		DeleteChildren( true );
		ActivePanel = panel;
		AddChild( panel );
		//this.SetClass( "fade-in", true );
	}

	public void Close()
	{
		if ( ActivePanel is null || _isForcedOpen )
			return;

		Log.Info("Closing panel");
		//this.EnableFade( false );
		DeleteChildren( true );
		ActivePanel = null;
		_isForcedOpen = false;
	}
	
}
