using System.Collections.Generic;
using Sandbox;
using SandboxEditor;

namespace Stroggle;

[Library( "stroggle_shopstation" ), HammerEntity]
[Title( "Shop Station" ), Category( "Gameplay" ), Icon( "shop" )]
public partial class ShopStation : ModelEntity, IUse
{
	private float useRange = 100;
	private List<Sandbox.Entity> users;
    public override void Spawn()
	{
		base.Spawn();
        SetModel( "models/rust_props/barrels/fuel_barrel.vmdl" );
		SetupPhysicsFromModel( PhysicsMotionType.Static, false );
		users = new List<Sandbox.Entity>();
    }

	public bool IsUsable( Entity user )
	{
		return true;
	}

    public bool OnUse( Entity user )
	{
		if ( user is not StrogglePlayer player )
			return false;

		if ( users.Contains(user) )
			return false;

		users.Add(user);
		OpenShopMenu();
		
		return false;
	}
	
	// We can't do purely UI stuff on the server so we tell the client to do it
	// This is what ClientRpc does, and why the hud in Game.cs is only executed if we're the client
	[ClientRpc]
	private void OpenShopMenu()
	{
		if ( UI.FullscreenUI.Instance.IsOpen ) {
			UI.FullscreenUI.Instance.Close();
		}
		UI.FullscreenUI.Instance?.Open( new UI.Shop() );
	}

	[ClientRpc]
	private void CloseShopMenu()
	{
		if ( UI.FullscreenUI.Instance.IsOpen ) {
			UI.FullscreenUI.Instance.Close();
		}
	}

	[Event.Tick.Server]
	protected void Tick()
	{
		List<Entity> usersToDelete = new List<Entity>();
		
		foreach (StrogglePlayer p in users)
		{
			if ( Vector3.DistanceBetween( p.Position, Position ) > useRange )
				usersToDelete.Add(p);
		}
		
		foreach (StrogglePlayer p in usersToDelete)
		{
			users.Remove(p);
			CloseShopMenu();
		}
	}
}