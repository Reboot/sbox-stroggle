using Sandbox;
using SandboxEditor;

namespace Stroggle;

partial class BaseAmmo : ModelEntity
{
	public virtual AmmoType AmmoType => AmmoType.None;
	public virtual int AmmoAmount => 17;
	public virtual Model WorldModel => Model.Load( "models/rust_props/small_junk/can.vmdl" );

	public override void Spawn()
	{
		base.Spawn();

		Model = WorldModel;

		PhysicsEnabled = true;
		UsePhysicsCollision = true;

		CollisionGroup = CollisionGroup.Weapon;
		SetInteractsAs( CollisionLayer.Debris );
	}

	public override void Touch( Entity other )
	{
		base.Touch( other );

		if ( other is not StrogglePlayer player )
			return;

		if ( other.LifeState != LifeState.Alive )
			return;

		var ammoTaken = player.GiveAmmo( AmmoType, AmmoAmount );

		if ( ammoTaken == 0 )
			return;

		Sound.FromWorld( "dm.pickup_ammo", Position );
		//PickupFeed.OnPickup( To.Single( player ), $"+{ammoTaken} {AmmoType}" );

		//ItemRespawn.Taken( this );
		Delete();
	}
}


[Library( "ammo_cell" ), HammerEntity]
[EditorModel( "models/rust_props/small_junk/can.vmdl" )]
[Title( "Cellpack" ), Category( "Ammo" )]
partial class AmmoCellpack : BaseAmmo
{
	public override AmmoType AmmoType => AmmoType.Cell;
	public override int AmmoAmount => 17;
	//public override Model WorldModel => Model.Load( "models/dm_ammo_9mmclip.vmdl" );

}